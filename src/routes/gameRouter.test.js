import request from "supertest"
import app from "../app"
import uuid from "uuid"
import fs from "fs"
// jest.mock("fs")
// import * as gamesService from "../services/gamesService"

const fakeId = "2366dbc9-1d62-48c3-94bc-7ba3912d42fa"
const fakeDate = new Date(10000000)

const createdGame = {
  id: "2366dbc9-1d62-48c3-94bc-7ba3912d42fa",
  currentPlayer: 1,
  moneyAvailable: 80,
  workers: [
    {
      id: 206,
      price: 3,
      stone: 0,
      wood: 0,
      knowledge: 1,
      tile: 2
    },
    {
      id: 207,
      price: 3,
      stone: 0,
      wood: 0,
      knowledge: 2,
      tile: 1
    },
    {
      id: 208,
      price: 3,
      stone: 0,
      wood: 1,
      knowledge: 0,
      tile: 2
    },
    {
      id: 209,
      price: 3,
      stone: 0,
      wood: 1,
      knowledge: 2,
      tile: 0
    },
    {
      id: 210,
      price: 3,
      stone: 0,
      wood: 2,
      knowledge: 0,
      tile: 1
    }
  ],
  buildings: [
    {
      id: 100,
      reward: 0,
      victoryPoint: 2,
      stone: 0,
      wood: 2,
      knowledge: 1,
      tile: 0,
      stoneProduced: 3,
      woodProduced: 0,
      knowledgeProduced: 0,
      tileProduced: 0
    },
    {
      id: 101,
      reward: 0,
      victoryPoint: 1,
      stone: 0,
      wood: 1,
      knowledge: 0,
      tile: 1,
      stoneProduced: 2,
      woodProduced: 0,
      knowledgeProduced: 0,
      tileProduced: 0
    },
    {
      id: 102,
      reward: 0,
      victoryPoint: 2,
      stone: 0,
      wood: 0,
      knowledge: 2,
      tile: 1,
      stoneProduced: 0,
      woodProduced: 3,
      knowledgeProduced: 0,
      tileProduced: 0
    },
    {
      id: 103,
      reward: 0,
      victoryPoint: 1,
      stone: 1,
      wood: 0,
      knowledge: 1,
      tile: 0,
      stoneProduced: 0,
      woodProduced: 2,
      knowledgeProduced: 0,
      tileProduced: 0
    },
    {
      id: 104,
      reward: 0,
      victoryPoint: 1,
      stone: 0,
      wood: 1,
      knowledge: 1,
      tile: 0,
      stoneProduced: 0,
      woodProduced: 0,
      knowledgeProduced: 0,
      tileProduced: 2
    }
  ],
  remainingWorkers: 35,
  remainingBuildings: 37,
  nextWorker: {
    id: 211,
    price: 3,
    stone: 0,
    wood: 2,
    knowledge: 1,
    tile: 0
  },
  nextBuilding: {
    id: 105,
    reward: 0,
    victoryPoint: 1,
    stone: 1,
    wood: 1,
    knowledge: 0,
    tile: 0,
    stoneProduced: 0,
    woodProduced: 0,
    knowledgeProduced: 2,
    tileProduced: 0
  },
  done: false,
  name: "Partie de test",
  createdDate: "1/1/1970, 3:46:40 AM",
  players: [
    {
      id: 1,
      finishedBuildings: [],
      availableWorkers: [
        {
          id: 200,
          price: 2,
          stone: 0,
          wood: 0,
          knowledge: 1,
          tile: 1
        }
      ],
      underConstructionBuildings: [],
      money: 10,
      victoryPoints: 0,
      actions: 3
    },
    {
      id: 2,
      finishedBuildings: [],
      availableWorkers: [
        {
          id: 201,
          price: 2,
          stone: 0,
          wood: 1,
          knowledge: 0,
          tile: 1
        }
      ],
      underConstructionBuildings: [],
      money: 10,
      victoryPoints: 0,
      actions: 0
    }
  ]
}

describe("/games", () => {
  test("should response the POST method", async () => {
    uuid.v4 = jest.fn().mockReturnValue(fakeId)
    const spy = jest.spyOn(global, "Date").mockImplementation(() => fakeDate)

    const response = await request(app)
      .post("/games")
      .type("json")
      .send({ name: "Partie de test", numberOfPlayers: 2, shuffleFlag: false })

    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual(createdGame)
    spy.mockRestore()
  })
  test("should return 500 if creating game failed", async () => {
    // gamesService.createGame = jest.fn().mockReturnValue(Error("Error test"))

    const response = await request(app)
      .post("/games")
      .type("json")
      .send({ name: "Bugged", numberOfPlayers: 24 })
    expect(response.statusCode).toBe(500)
    expect(response.text).toEqual("Can't create a new game.")
  })
})

const testDatabase2 = `[{"id":"5b25c957-e0e2-4b38-b1e8-8055f9b004ca", "name": "Partie pas folle", "done":false,"createdDate":"2020-11-19T16:01:09.759Z","players":["Player1","Player2"], "_private":"decks","champBidon":12,"champBidon2":"vide"},{"id": "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c","name": "Partie de malade", "done":true,"createdDate":"2020-11-19T16:01:09.759Z","players":["Player1","Player2","Player3"],"_private": "decks","champBidon":24,"champBidon2":"trucs"}]`

describe("GET /games", () => {
  test("should return 500 if the database doesn't exists yet", async () => {
    fs.promises.readFile = jest.fn().mockReturnValue(Error("Error Test"))

    const response = await request(app).get("/games")
    expect(response.statusCode).toBe(500)
    expect(response.text).toEqual("Can't show all games.")
  })

  test("should return the game info", async () => {
    fs.promises.readFile = jest.fn().mockReturnValue(testDatabase2)

    const response = await request(app).get("/games")
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual([
      {
        numberOfPlayers: 2,
        id: "5b25c957-e0e2-4b38-b1e8-8055f9b004ca",
        name: "Partie pas folle",
        done: false,
        createdDate: "2020-11-19T16:01:09.759Z"
      },
      {
        numberOfPlayers: 3,
        id: "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c",
        name: "Partie de malade",
        done: true,
        createdDate: "2020-11-19T16:01:09.759Z"
      }
    ])
  })
})

const testDatabase = `\
[{"id":"5b25c957-e0e2-4b38-b1e8-8055f9b004ca", "player":2, "_private":"decks"},{"id": "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c","player": 3,"_private": "decks"}]`

describe("/games/{gameId}", () => {
  test("should return 404 if the game doesn't exist", async () => {
    fs.promises.readFile = jest.fn().mockReturnValue(testDatabase)
    const response = await request(app).get("/games/badId")
    expect(response.statusCode).toBe(404)
    expect(response.text).toEqual("Can't find game.")
  })

  test("should return the game info", async () => {
    fs.promises.readFile = jest.fn().mockReturnValue(testDatabase)
    const response = await request(app).get(
      "/games/5b25c957-e0e2-4b38-b1e8-8055f9b004ca"
    )
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual({
      id: "5b25c957-e0e2-4b38-b1e8-8055f9b004ca",
      player: 2
    })
  })
})
