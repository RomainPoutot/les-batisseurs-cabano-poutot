import express from "express"
import HttpError from "../middlewares/HttpError"
import * as gamesService from "../services/gamesService"
import * as actionService from "../services/actionService"
const router = express.Router()
const actions = {
  takeBuilding: "TAKE_BUILDING",
  takeWorker: "TAKE_WORKER",
  takeMoney: "TAKE_MONEY",
  sendWorker: "SEND_WORKER",
  buyAction: "BUY_ACTION",
  endTurn: "END_TURN"
}

// Accessible via http://localhost:3000/games */
router.post("", async function(req, res) {
  const numberOfPlayers = req.body.numberOfPlayers
  const name = req.body.name
  const shuffleFlag = req.body.shuffleFlag
  try {
    gamesService.checkParams(numberOfPlayers, name)
    const newGame = await gamesService.createGame(
      numberOfPlayers,
      name,
      shuffleFlag
    )
    await gamesService.saveGameToDatabase(newGame)
    res.json(gamesService.filterPrivateField(newGame))
  } catch (e) {
    // Retourne une erreur 500 si la lecture à échouée
    throw new HttpError(500, "Can't create a new game.")
  }
})

// Accessible via http://localhost:3000/games */
router.get("", async function(req, res) {
  try {
    const database = await gamesService.readDatabase()
    res.json(gamesService.listAllGames(database))
  } catch (e) {
    // Retourne une erreur 500 si la lecture à échouée
    throw new HttpError(500, "Can't show all games.")
  }
})

// Accessible via http://localhost:3000/games/{gameId} */
router.get("/:gameId", async function(req, res) {
  try {
    const id = req.params.gameId
    const gameInfo = await gamesService.findGameById(id)
    res.json(gamesService.filterPrivateField(gameInfo))
  } catch (e) {
    throw new HttpError(404, "Can't find game.")
  }
})

// Accessible via http://localhost:3000/games/{gameId}/actions */
router.post("/:gameId/actions", async function(req, res) {
  const id = req.params.gameId
  const playerId = parseInt(req.headers["player-id"])
  const data = req.body

  const gameInfo = await gamesService.findGameById(id)
  if (actionService.checkPlayerTurn(playerId, gameInfo)) {
    let game
    switch (data.type) {
      // Action TAKE_BUILDING
      case actions.takeBuilding:
        game = actionService.takeBuilding(
          playerId,
          parseInt(data.payload.buildingId),
          gameInfo
        )
        break

      // Action TAKE_WORKER
      case actions.takeWorker:
        game = actionService.takeWorker(
          playerId,
          parseInt(data.payload.workerId),
          gameInfo
        )
        break

      // Action TAKE_MONEY
      case actions.takeMoney:
        game = actionService.takeMoney(
          playerId,
          data.payload.numberOfActions,
          gameInfo
        )
        break

      // Action SEND_WORKER
      case actions.sendWorker:
        game = actionService.sendWorker(
          playerId,
          parseInt(data.payload.buildingId),
          parseInt(data.payload.workerId),
          gameInfo
        )
        game = actionService.handleFinishedBuilding(
          playerId,
          parseInt(data.payload.buildingId),
          gameInfo
        )
        break

      // Action BUY_ACTION
      case actions.buyAction:
        game = actionService.buyAction(
          playerId,
          data.payload.numberOfActions,
          gameInfo
        )
        break

      // Action END_TURN
      case actions.endTurn:
        game = actionService.endTurn(playerId, gameInfo)
        game = actionService.gameFinished(playerId, game)
        break
    }
    if (req.body.test) {
      await gamesService.updateDatabase(game, "../../storage/databasetest.json")
    } else {
      await gamesService.updateDatabase(game)
    }

    res.json(game)
  } else {
    throw new HttpError(401, "It's not the player turn.")
  }
})
export default router
