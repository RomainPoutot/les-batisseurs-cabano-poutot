import request from "supertest"
import app from "../app"
import * as gamesService from "../services/gamesService"

// Actions to test :
// gameid : ab13df77-4bc7-438e-b962-962c6b012c8d
// - it's not the player's turn
// - TAKE_WORKER                                X
//    - not enough actions                      -> J4 13    OK
//    - worker not available                    -> J4 7     OK
//    - request sucessful                       -> J4 8     OK
// - TAKE_BUILDING                              X
//    - not enough actions                      -> J4 14    OK
//    - building not available                  -> J4 9     OK
//    - request sucessful                       -> J4 10    OK
// - SEND_WORKER                                X
//    - not enough actions                      -> J1 17
//    - worker not available                    -> J1 14    OK
//    - building not available                  -> J1 15    OK
//    - not enough money                        -> J3 5     OK
//    - request successful                      X
//      - Building not finished                 -> J4 11    OK
//      - Building finished                     -> J4 12    OK
//      - building finished and it is a machine -> J1 16    OK
// - TAKE_MONEY                                 X
//    - not enough actions                      -> J3 2     OK
//    - request successful                      -> J3 1     OK
// - BUY_ACTION                                 X
//    - not enough money                        -> J3 4     OK
//    - request successful                      -> J3 3     OK
// - END_TURN                                   X
//    - request successful                      X
//      - game is over                          -> J1 18
//      - game isn't over                       -> J3 6 / J4 13 OK

let fakeGame
let cleanGame

// beforeEach(() => {
//   gamesService.findGameById = jest.fn().mockReturnValue(fakeGame)
// })

describe("/games/{gameId}/actions", () => {
  test("Should return 401 if a player try to play during someone's else turn", async () => {
    //
    cleanGame = await gamesService.readDatabase(
      "../../storage/databasetest.json"
    )
    cleanGame = cleanGame[0]
    fakeGame = await gamesService.readDatabase(
      "../../storage/databasetest.json"
    )
    fakeGame = fakeGame[0]
    gamesService.findGameById = jest.fn().mockReturnValue(fakeGame)
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "1")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: "109",
          workerId: "222"
        },
        test: true
      })
    expect(response.statusCode).toBe(401)
    expect(response.text).toStrictEqual("It's not the player turn.")
  })

  test("TAKE_MONEY : Should update player's money / actions and availableMoney ", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "TAKE_MONEY",
        payload: {
          numberOfActions: 1
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[2].money).toStrictEqual(11)
    expect(fakeGame.players[2].actions).toStrictEqual(1)
    expect(fakeGame.moneyAvailable).toStrictEqual(59)
  })

  test("TAKE_MONEY : Should return 400 when not enough actions or money available", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "TAKE_MONEY",
        payload: {
          numberOfActions: 2
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Can't execute this action. Not enough money or actions available "
    )
    expect(fakeGame.players[2].money).toStrictEqual(11)
    expect(fakeGame.players[2].actions).toStrictEqual(1)
    expect(fakeGame.moneyAvailable).toStrictEqual(59)
  })

  test("BUY_ACTION : Should update player's money / actions ", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "BUY_ACTION",
        payload: {
          numberOfActions: 2
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[2].money).toStrictEqual(1)
    expect(fakeGame.players[2].actions).toStrictEqual(3)
    expect(fakeGame.moneyAvailable).toStrictEqual(69)
  })

  test("BUY_ACTION : Should return 400 when the player doesn't have enough money", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "BUY_ACTION",
        payload: {
          numberOfActions: 1
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Can't execute this action. Not enough money"
    )
    expect(fakeGame.players[2].money).toStrictEqual(1)
    expect(fakeGame.players[2].actions).toStrictEqual(3)
    expect(fakeGame.moneyAvailable).toStrictEqual(69)
  })

  test("SEND_WORKER : Should return 400 when the player doesn't have enough money to send a worker", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          workerId: 204,
          buildingId: 117
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Worker/Building isn't available or not enough money"
    )
    expect(fakeGame.players[2].money).toStrictEqual(1)
    expect(fakeGame.players[2].actions).toStrictEqual(3)
    expect(fakeGame.players[2].availableWorkers.length).toStrictEqual(1)
  })

  test("END_TURN : Should change CurrentPlayer to player 4 ( game not finished )", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "END_TURN",
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.currentPlayer).toStrictEqual(4)
    expect(fakeGame.players[3].actions).toStrictEqual(3)
    expect(fakeGame.done).toStrictEqual(false)
  })

  test("TAKE_WORKER : Should return 400 when the worker isn't on the field", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "TAKE_WORKER",
        payload: {
          workerId: 8000
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(fakeGame.players[3].actions).toStrictEqual(3)
    expect(fakeGame.players[3].availableWorkers.length).toStrictEqual(1)
  })

  test("TAKE_WORKER : Should add the worker to the availableWorkers list and update the board", async () => {
    const incomingWorker = fakeGame.nextWorker
    const incomingNextWorker = fakeGame._private.workersDeck[0]
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "TAKE_WORKER",
        payload: {
          workerId: 224
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[3].actions).toStrictEqual(2)
    expect(fakeGame.players[3].availableWorkers.length).toStrictEqual(2)
    expect(fakeGame.workers[4]).toStrictEqual(incomingWorker)
    expect(fakeGame.nextWorker).toStrictEqual(incomingNextWorker)
  })

  test("TAKE_BUILDING : Should return 400 when the building isn't on the field", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "TAKE_BUILDING",
        payload: {
          buildingId: 8000
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(fakeGame.players[3].actions).toStrictEqual(2)
    expect(fakeGame.players[3].underConstructionBuildings.length).toStrictEqual(
      1
    )
  })

  test("TAKE_BUILDING : Should add the building to the underConstructionBuildings list and update the board", async () => {
    const incomingBuilding = fakeGame.nextBuilding
    const incomingNextBuilding = fakeGame._private.buildingsDeck[0]
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "TAKE_BUILDING",
        payload: {
          buildingId: 138
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[3].actions).toStrictEqual(1)
    expect(fakeGame.players[3].underConstructionBuildings.length).toStrictEqual(
      2
    )
    expect(fakeGame.buildings[0]).toStrictEqual(incomingBuilding)
    expect(fakeGame.nextBuilding).toStrictEqual(incomingNextBuilding)
  })

  test("SEND_WORKER x Building not finished : Should add the worker to the workers list of the building, remove it from availableWorkers and update the needed ressources", async () => {
    const building = fakeGame.players[3].underConstructionBuildings[0]
    const sentWorker = fakeGame.players[3].availableWorkers[1]
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: 111,
          workerId: 224
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[3].actions).toStrictEqual(0)
    expect(building.workers.length).toStrictEqual(1)
    expect(building.workers[0]).toStrictEqual(sentWorker)
    expect(building.stone).toStrictEqual(0)
    expect(building.wood).toStrictEqual(0)
    expect(building.knowledge).toStrictEqual(0)
    expect(building.tile).toStrictEqual(1)
    expect(fakeGame.players[3].availableWorkers.length).toStrictEqual(1)
    expect(fakeGame.players[3].money).toStrictEqual(6)
  })

  test("SEND_WORKER : Should return 400 when the player doesn't have enough actions", async () => {
    const building = fakeGame.players[3].underConstructionBuildings[0]
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: 111,
          workerId: 201
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Can't execute this action. Not enough actions available ! "
    )
    expect(fakeGame.players[3].actions).toStrictEqual(0)
    expect(building.workers.length).toStrictEqual(1)
    expect(building.stone).toStrictEqual(0)
    expect(building.wood).toStrictEqual(0)
    expect(building.knowledge).toStrictEqual(0)
    expect(building.tile).toStrictEqual(1)
    expect(fakeGame.players[3].availableWorkers.length).toStrictEqual(1)
  })

  test(`SEND_WORKER x Building finished : Should add the worker to the workers list of the building,\
     remove it from availableWorkers and update the needed ressources\
     action cost should also be higher since a worker has already been sent`, async () => {
    fakeGame.players[3].actions += 2
    gamesService.updateDatabase(fakeGame, "../../storage/databasetest.json")
    const building = fakeGame.players[3].underConstructionBuildings[0]
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: 111,
          workerId: 201
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[3].actions).toStrictEqual(0)
    expect(building.workers.length).toStrictEqual(0)
    expect(building.stone).toStrictEqual(0)
    expect(building.wood).toStrictEqual(0)
    expect(building.knowledge).toStrictEqual(0)
    expect(building.tile).toStrictEqual(0)
    expect(fakeGame.players[3].availableWorkers.length).toStrictEqual(2)
    expect(fakeGame.players[3].finishedBuildings).toStrictEqual([building])
    expect(fakeGame.players[3].victoryPoints).toStrictEqual(1)
    expect(fakeGame.players[3].money).toStrictEqual(10)
  })

  test("TAKE_WORKER : Should return 400 when the player has no actions left", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "TAKE_WORKER",
        payload: {
          workerId: 220
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Can't execute this action. Not enough actions available ! "
    )
    expect(fakeGame.players[3].actions).toStrictEqual(0)
    expect(fakeGame.players[3].availableWorkers.length).toStrictEqual(2)
  })

  test("TAKE_BUILDING : Should return 400 when the player has no actions left", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "TAKE_BUILDING",
        payload: {
          buildingId: 104
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Can't execute this action. Not enough actions available ! "
    )
    expect(fakeGame.players[3].actions).toStrictEqual(0)
    expect(fakeGame.players[3].underConstructionBuildings.length).toStrictEqual(
      1
    )
  })

  test("END_TURN : Should change CurrentPlayer to player 1 ( game not finished )", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "END_TURN",
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.currentPlayer).toStrictEqual(1)
    expect(fakeGame.players[0].actions).toStrictEqual(3)
    expect(fakeGame.done).toStrictEqual(false)
  })

  test("SEND_WORKER : Should return 400 if the worker isn't available", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "1")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: 103,
          workerId: 8000
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Worker/Building isn't available or not enough money"
    )
    expect(fakeGame.players[0].actions).toStrictEqual(3)
    expect(fakeGame.players[0].availableWorkers.length).toStrictEqual(2)
  })

  test("SEND_WORKER : Should return 400 if the building isn't available", async () => {
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "1")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: 8000,
          workerId: 230
        },
        test: true
      })
    expect(response.statusCode).toBe(400)
    expect(response.text).toStrictEqual(
      "Worker/Building isn't available or not enough money"
    )
    expect(fakeGame.players[0].actions).toStrictEqual(3)
    expect(fakeGame.players[0].availableWorkers.length).toStrictEqual(2)
  })

  test(`SEND_WORKER x Building finished : Should add the worker to the workers list of the building,\
  remove it from availableWorkers and update the needed ressources, \
  Since the building was a machine it should be added to the availableWorkers`, async () => {
    const building = fakeGame.players[0].underConstructionBuildings[0]
    const machine = {
      id: 103,
      price: 0,
      stone: 0,
      wood: 2,
      tile: 0,
      knowledge: 0
    }
    const response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "1")
      .type("json")
      .send({
        type: "SEND_WORKER",
        payload: {
          buildingId: 103,
          workerId: 230
        },
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.players[0].actions).toStrictEqual(2)
    expect(fakeGame.players[0].availableWorkers.length).toStrictEqual(3)
    expect(building.workers.length).toStrictEqual(0)
    expect(building.stone).toStrictEqual(0)
    expect(building.wood).toStrictEqual(0)
    expect(building.knowledge).toStrictEqual(0)
    expect(building.tile).toStrictEqual(0)
    expect(fakeGame.players[0].availableWorkers[2]).toStrictEqual(machine)
    expect(fakeGame.players[0].finishedBuildings).toStrictEqual([building])
    expect(fakeGame.players[0].victoryPoints).toStrictEqual(17)
    expect(fakeGame.players[0].money).toStrictEqual(6)
  })

  test("END_TURN : Should change CurrentPlayer to player 2 ( last turn )", async () => {
    let response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "1")
      .type("json")
      .send({
        type: "END_TURN",
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.currentPlayer).toStrictEqual(2)
    expect(fakeGame.players[1].actions).toStrictEqual(3)
    expect(fakeGame.done).toStrictEqual(false)
    response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "2")
      .type("json")
      .send({
        type: "END_TURN",
        test: true
      })
    response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "3")
      .type("json")
      .send({
        type: "END_TURN",
        test: true
      })
    // Player 4 is the last one to play
    response = await request(app)
      .post("/games/ab13df77-4bc7-438e-b962-962c6b012c8d/actions")
      .set("player-id", "4")
      .type("json")
      .send({
        type: "END_TURN",
        test: true
      })
    expect(response.statusCode).toBe(200)
    expect(fakeGame.done).toStrictEqual(true)
    expect(fakeGame.players[1].victoryPoints).toStrictEqual(10)

    // A faire à la fin du dernier test !!!
    await gamesService.updateDatabase(
      cleanGame,
      "../../storage/databasetest.json"
    )
  })
})
