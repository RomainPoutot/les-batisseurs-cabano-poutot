import uuid from "uuid"
import { importBuildings, importWorkers } from "./cardService"
import * as actionService from "./actionService"
import HttpError from "../middlewares/HttpError"
import fs from "fs"
import { shuffle } from "lodash"
import path from "path"

export function createPlayer(id, ouvrier) {
  let act = 0
  if (id === 0) {
    act = 3
  }
  const player = {
    id: id + 1,
    finishedBuildings: [],
    availableWorkers: [ouvrier],
    underConstructionBuildings: [],
    money: 10,
    victoryPoints: 0,
    actions: act
  }
  return player
}

export function getApprenti(ouvriers, nbPlayers, shuffleFlag) {
  let apprentisList = ouvriers.slice(0, 5)
  if (shuffleFlag) {
    apprentisList = shuffle(apprentisList)
  }
  const res = apprentisList.slice(0, nbPlayers)
  const newWorkers = apprentisList.slice(nbPlayers)
  ouvriers = ouvriers.slice(6).concat(newWorkers)
  const tmp = { res: res, workers: ouvriers }
  return tmp
}

export async function createGame(numberOfPlayers, name, shuffleFlag = true) {
  const playerlist = []
  const workers = await importWorkers()
  let allBuildings = await importBuildings()
  const tmp = getApprenti(workers, numberOfPlayers, shuffleFlag)
  const starterApprentis = tmp.res
  let allWorkers = tmp.workers
  for (let i = 0; i < numberOfPlayers; i++) {
    playerlist[i] = createPlayer(i, starterApprentis[i])
  }

  if (shuffleFlag) {
    allWorkers = shuffle(allWorkers)
    allBuildings = shuffle(allBuildings)
  }

  const game = {
    id: uuid.v4(),
    currentPlayer: 1,
    moneyAvailable: 25 + 5 * 15 - numberOfPlayers * 10,
    workers: allWorkers.slice(0, 5),
    buildings: allBuildings.slice(0, 5),
    remainingWorkers: 37 - numberOfPlayers,
    remainingBuildings: 37,
    nextWorker: allWorkers[5],
    nextBuilding: allBuildings[5],
    done: false,
    name: name,
    createdDate: new Date().toLocaleString(),
    players: playerlist,
    _private: {
      workersDeck: allWorkers.slice(6),
      buildingsDeck: allBuildings.slice(6)
    }
  }
  return game
}

export function checkParams(numberOfPlayers, name) {
  if (numberOfPlayers < 2 || numberOfPlayers > 4 || name === "") {
    throw new Error()
  }
}

export function filterPrivateField(obj) {
  delete obj._private
  return obj
}

export async function saveGameToDatabase(game) {
  try {
    await fs.promises.mkdir(path.join(__dirname, "../../storage"))
  } catch (e) {
    // if directory already exists
  }
  const databasePath = path.join(__dirname, "../../storage/database.json")
  let database = []
  try {
    const file = await fs.promises.readFile(databasePath)
    database = JSON.parse(file)
  } catch (e) {
    // if file not exists
  }
  database.push(game)
  await fs.promises.writeFile(databasePath, JSON.stringify(database))
}

export async function readDatabase(location = "../../storage/database.json") {
  const databasePath = path.join(__dirname, location)
  let database
  try {
    const file = await fs.promises.readFile(databasePath)
    database = JSON.parse(file)
  } catch (e) {
    throw new Error()
  }
  return database
}

export async function updateDatabase(
  game,
  location = "../../storage/database.json"
) {
  try {
    const databasePath = path.join(__dirname, location)
    const database = await readDatabase(location)
    database[actionService.findById(game.id, database)] = game
    await fs.promises.writeFile(databasePath, JSON.stringify(database))
  } catch (e) {
    throw new HttpError(400, "This game isn't in the database")
  }
}

export function listAllGames(database) {
  for (let i = 0; i < database.length; i++) {
    database[i] = {
      numberOfPlayers: database[i].players.length,
      id: database[i].id,
      name: database[i].name,
      done: database[i].done,
      createdDate: database[i].createdDate
    }
  }
  return database
}

export async function findGameById(gameId) {
  const database = await readDatabase()
  const infoGame = database.find(game => game.id === gameId)
  if (infoGame === []) {
    throw new Error()
  }
  return infoGame
}
