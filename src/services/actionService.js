import HttpError from "../middlewares/HttpError"
// Variable globale pour terminer une partie
let lastTurn = false

export function takeBuilding(playerId, buildingId, game) {
  const playerIndex = findById(playerId, game.players)
  if (game.players[playerIndex].actions > 0) {
    // Making sure the building is available on the field
    const buildingIndex = findById(buildingId, game.buildings)
    if (buildingIndex !== -1) {
      // Adding new key/value pair to the building
      // One to remember workers working on this building
      // And one to know how much actions it will cost to send a worker
      const wantedBuilding = game.buildings[buildingIndex]
      wantedBuilding.workers = []
      wantedBuilding.numberOfTimes = 0
      // Adding the building to the player's underConstructionBuildings
      game.players[playerIndex].underConstructionBuildings.push(wantedBuilding)
      // Removing the building from the pool of available buildings
      // And adding the following building to the pool
      game.buildings.splice(buildingIndex, 1, game.nextBuilding)
      // Updating the following building
      game.nextBuilding = game._private.buildingsDeck[0]
      // Removing it from buildingDeck and updating remainingBuildings
      game._private.buildingsDeck.splice(0, 1)
      game.remainingBuildings -= 1
      // Updating player's remaining actions
      game.players[playerIndex].actions -= 1
    } else {
      throw new HttpError(
        400,
        "Can't execute this action. Building isn't available"
      )
    }
  } else {
    throw new HttpError(
      400,
      "Can't execute this action. Not enough actions available ! "
    )
  }
  return game
}

export function takeWorker(playerId, workerId, game) {
  const playerIndex = findById(playerId, game.players)
  if (game.players[playerIndex].actions > 0) {
    // Making sure the worker is available on the field
    const workerIndex = findById(workerId, game.workers)
    if (workerIndex !== -1) {
      // Adding the worker to the player's availableWorkers
      game.players[playerIndex].availableWorkers.push(game.workers[workerIndex])
      // Removing the worker from the pool of available workers
      // And adding the following worker to the pool
      game.workers.splice(workerIndex, 1, game.nextWorker)
      // Updating the following worker
      game.nextWorker = game._private.workersDeck[0]
      // Removing it from workersDeck and updating remainingWorkers
      game._private.workersDeck.splice(0, 1)
      game.remainingWorkers -= 1
      // Updating player's remaining actions
      game.players[playerIndex].actions -= 1
    } else {
      throw new HttpError(
        400,
        "Can't execute this action. Worker isn't available"
      )
    }
  } else {
    throw new HttpError(
      400,
      "Can't execute this action. Not enough actions available ! "
    )
  }
  return game
}

export function sendWorker(playerId, buildingId, workerId, game) {
  const playerIndex = findById(playerId, game.players)
  const player = game.players[playerIndex]
  // Checking if the player still has actions left
  if (player.actions > 0) {
    // Getting the index of the worker he wants to use
    const workerIndex = findById(workerId, player.availableWorkers)
    // Getting the index of the building
    const buildingIndex = findById(
      buildingId,
      player.underConstructionBuildings
    )
    const worker = player.availableWorkers[workerIndex]
    const building = player.underConstructionBuildings[buildingIndex]
    // Checking if both are available and if he has enough coins to pay
    if (
      workerIndex !== -1 &&
      buildingIndex !== -1 &&
      worker.price <= player.money
    ) {
      // The number of times a worker has been sent to this building
      // during this turn only to determine how many actions will be needed to send the worker
      const numberOfTimes = building.numberOfTimes
      // Checking if the player has enough actions
      if (player.actions > numberOfTimes) {
        player.money -= worker.price
        player.actions -= numberOfTimes + 1
        building.numberOfTimes += 1
        // Removing the worker from the availableWorker list
        // Adding him into the workers array in the building to be remembered
        // to be remembered once the building is finished
        building.workers.push(worker)
        player.availableWorkers.splice(workerIndex, 1)
        // Update building needed ressources
        // NOT MANDATORY, IF BAD IDEA JUST COMMENT THE FOLLOWING LINE
        updateRessources(building, worker)
      } else {
        throw new HttpError(
          400,
          "Can't execute this action. Not enough actions available ! "
        )
      }
    } else {
      throw new HttpError(
        400,
        "Worker/Building isn't available or not enough money"
      )
    }
  } else {
    throw new HttpError(
      400,
      "Can't execute this action. Not enough actions available ! "
    )
  }
  return game
}

export function takeMoney(playerId, nbActions, game) {
  const price = [
    { actions: 1, money: 1 },
    { actions: 2, money: 3 },
    { actions: 3, money: 6 }
  ]
  const playerIndex = findById(playerId, game.players)
  const actualPrice = price.find(element => element.actions === nbActions)
  return updateMoney(playerIndex, actualPrice, game)
}

export function endTurn(playerId, game) {
  const playerIndex = findById(playerId, game.players)
  const player = game.players[playerIndex]
  // Setting the numberOfTimes variable to 0 for each building of the player
  player.underConstructionBuildings.forEach(element => {
    element.numberOfTimes = 0
  })
  // Setting the currentPlayer variable to the id of the next player
  game.currentPlayer = (game.currentPlayer % game.players.length) + 1
  // Giving him 3 actions
  const newPlayerIndex = findById(game.currentPlayer, game.players)
  game.players[newPlayerIndex].actions = 3
  return game
}

export function buyAction(playerId, nbActions, game) {
  const playerIndex = findById(playerId, game.players)
  if (game.players[playerIndex].money >= 5 * nbActions) {
    game.players[playerIndex].actions += 1 * nbActions
    game.players[playerIndex].money -= 5 * nbActions
    game.moneyAvailable += 5 * nbActions
  } else {
    throw new HttpError(400, "Can't execute this action. Not enough money")
  }
  return game
}

export function findById(id, array) {
  return array.findIndex(element => element.id === id)
}

export function checkPlayerTurn(playerId, game) {
  return game.currentPlayer === playerId
}

export function updateRessources(building, worker) {
  const toUpdate = ["stone", "wood", "knowledge", "tile"]
  toUpdate.forEach(element => {
    if (building[element] > 0 && worker[element] > 0) {
      building[element] = Math.max(0, building[element] - worker[element])
    }
  })
}

export function updateMoney(playerIndex, actualPrice, game) {
  const player = game.players[playerIndex]
  if (
    game.moneyAvailable >= actualPrice.money &&
    player.actions >= actualPrice.actions
  ) {
    player.actions -= actualPrice.actions
    player.money += actualPrice.money
    game.moneyAvailable -= actualPrice.money
  } else {
    throw new HttpError(
      400,
      "Can't execute this action. Not enough money or actions available "
    )
  }
  return game
}

export function handleFinishedBuilding(playerId, buildingId, game) {
  // Getting the index of the player
  const playerIndex = findById(playerId, game.players)
  const player = game.players[playerIndex]
  // Getting the index of the building
  const buildingIndex = findById(buildingId, player.underConstructionBuildings)
  const building = player.underConstructionBuildings[buildingIndex]
  // Checking if the building is finished
  if (isFinished(playerId, buildingId, game)) {
    // Adding the workers used back to availableWorkers
    const workersUsed = building.workers
    workersUsed.forEach(element => player.availableWorkers.push(element))
    building.workers = []
    // Adding his well earned victory points / coins
    player.victoryPoints += building.victoryPoint
    if (building.reward !== 0) {
      player.money += building.reward
    } else {
      // if it's a machine, adding him to the availableWorkers
      player.availableWorkers.push({
        id: building.id,
        price: 0,
        stone: building.stoneProduced,
        wood: building.woodProduced,
        knowledge: building.knowledgeProduced,
        tile: building.tileProduced
      })
    }
    // adding him into the finished buildings
    player.finishedBuildings.push(building)
    player.underConstructionBuildings.splice(buildingIndex, 1)
  }
  return game
}

export function isFinished(playerId, buildingId, game) {
  // Getting the index of the player
  const playerIndex = findById(playerId, game.players)
  const player = game.players[playerIndex]
  // Getting the index of the building
  const buildingIndex = findById(buildingId, player.underConstructionBuildings)
  const building = player.underConstructionBuildings[buildingIndex]
  return (
    building.stone === 0 &&
    building.wood === 0 &&
    building.knowledge === 0 &&
    building.tile === 0
  )
}

export function updateVictoryPointsWithCoins(game) {
  let nbVictoryPoints
  let player
  // Updating Victory Points
  for (let index = 0; index < game.players.length; index++) {
    player = game.players[index]
    nbVictoryPoints = Math.trunc(player.money / 10)
    player.victoryPoints += nbVictoryPoints
  }
  return game
}

export function gameFinished(playerId, game) {
  // Checking the conditions & updating victory points for the current player
  const playerIndex = findById(playerId, game.players)
  const player = game.players[playerIndex]
  if (player.victoryPoints >= 17 || lastTurn) {
    lastTurn = true
    if (playerId === game.players.length) {
      game.done = true
      updateVictoryPointsWithCoins(game)
    }
  }
  return game
}
