import * as gamesService from "./gamesService"
import fs from "fs"
jest.mock("fs")

const testDatabase = `\
[{"id":"5b25c957-e0e2-4b38-b1e8-8055f9b004ca", "player":2, "_private":"decks"},{"id": "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c","player": 3,"_private": "decks"}]`
const testDatabase2 = `[{"id":"5b25c957-e0e2-4b38-b1e8-8055f9b004ca", "name": "Partie pas folle", "done":false,"createdDate":"2020-11-19T16:01:09.759Z","players":["Player1","Player2"], "_private":"decks","champBidon":12,"champBidon2":"vide"},{"id": "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c","name": "Partie de malade", "done":true,"createdDate":"2020-11-19T16:01:09.759Z","players":["Player1","Player2","Player3"],"_private": "decks","champBidon":24,"champBidon2":"trucs"}]`

describe("List all games", () => {
  test("should list numberOfPlayers/id/name/done/createdDate of all games in database", async () => {
    fs.promises = {
      readFile: jest.fn().mockResolvedValue(testDatabase2)
    }
    const database = await gamesService.readDatabase()
    expect(gamesService.listAllGames(database)).toStrictEqual([
      {
        numberOfPlayers: 2,
        id: "5b25c957-e0e2-4b38-b1e8-8055f9b004ca",
        name: "Partie pas folle",
        done: false,
        createdDate: "2020-11-19T16:01:09.759Z"
      },
      {
        numberOfPlayers: 3,
        id: "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c",
        name: "Partie de malade",
        done: true,
        createdDate: "2020-11-19T16:01:09.759Z"
      }
    ])
  })
})

describe("readDatabase", () => {
  test("should read the database", async () => {
    fs.promises = {
      readFile: jest.fn().mockResolvedValue(testDatabase)
    }
    const games = await gamesService.readDatabase()

    expect(games).toStrictEqual([
      {
        id: "5b25c957-e0e2-4b38-b1e8-8055f9b004ca",
        player: 2,
        _private: "decks"
      },
      {
        id: "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c",
        player: 3,
        _private: "decks"
      }
    ])
  })
})

describe("findGameById", () => {
  test("should find the info of a game by its ID", async () => {
    fs.promises = {
      readFile: jest.fn().mockResolvedValue(testDatabase)
    }

    const gameInfo = await gamesService.findGameById(
      "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c"
    )
    expect(gameInfo).toStrictEqual({
      id: "f7c44a5a-096b-4913-9b0b-41e8a8a1ca8c",
      player: 3,
      _private: "decks"
    })
  })
})
