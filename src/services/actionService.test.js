import * as actionService from "./actionService"
import HttpError from "../middlewares/HttpError"
jest.mock("fs")

// OBJECT USED FOR FUNCTION TESTING
const worker = {
  id: 235,
  price: 5,
  stone: 0,
  wood: 0,
  knowledge: 3,
  tile: 2
}
const worker2 = {
  id: 236,
  price: 5,
  stone: 0,
  wood: 0,
  knowledge: 3,
  tile: 2
}
const fakeGame = {
  moneyAvailable: 4,
  players: [
    { id: 1, actions: 2, money: 0 },
    { id: 2, actions: 1, money: 0 }
  ]
}
const fakeGame2 = {
  players: [
    {
      id: 1,
      victoryPoints: 0,
      money: 0,
      finishedBuildings: [],
      availableWorkers: [],
      underConstructionBuildings: [
        {
          id: 100,
          reward: 5,
          victoryPoint: 2,
          stone: 0,
          wood: 0,
          knowledge: 0,
          tile: 0,
          stoneProduced: 0,
          woodProduced: 0,
          knowledgeProduced: 0,
          tileProduced: 0,
          workers: [worker],
          numberOfTimes: 0
        },
        {
          id: 101,
          reward: 0,
          victoryPoint: 2,
          stone: 0,
          wood: 0,
          knowledge: 0,
          tile: 0,
          stoneProduced: 0,
          woodProduced: 3,
          knowledgeProduced: 0,
          tileProduced: 0,
          workers: [worker2],
          numberOfTimes: 0
        }
      ]
    },
    {
      id: 2,
      finishedBuildings: [],
      availableWorkers: [],
      underConstructionBuildings: [
        { id: 101, stone: 2, wood: 0, knowledge: 2, tile: 0 }
      ]
    }
  ]
}

const building = {
  id: 120,
  reward: 12,
  victoryPoint: 3,
  stone: 0,
  wood: 2,
  knowledge: 2,
  tile: 2,
  stoneProduced: 0,
  woodProduced: 0,
  knowledgeProduced: 0,
  tileProduced: 0,
  workers: [],
  numberOfTimes: 0
}

// END OF DEFINITIONS

describe("Test auxiliary functions", () => {
  test("UpdateRessources : should update the building's needed ressources once a worker has been sent", async () => {
    actionService.updateRessources(building, worker)
    expect(building).toStrictEqual({
      id: 120,
      reward: 12,
      victoryPoint: 3,
      stone: 0,
      wood: 2,
      knowledge: 0,
      tile: 0,
      stoneProduced: 0,
      woodProduced: 0,
      knowledgeProduced: 0,
      tileProduced: 0,
      workers: [],
      numberOfTimes: 0
    })
  })
})
describe("UpdateMoney", () => {
  test("UpdateMoney : should update player's money and actions when sending a worker to a building", async () => {
    const game = actionService.updateMoney(
      0,
      { actions: 2, money: 3 },
      fakeGame
    )
    expect(game).toStrictEqual({
      moneyAvailable: 1,
      players: [
        { id: 1, actions: 0, money: 3 },
        { id: 2, actions: 1, money: 0 }
      ]
    })
    // Should fail because not enough actions
    expect(() => {
      actionService.updateMoney(0, { actions: 2, money: 3 }, game)
    }).toThrow(
      new HttpError(
        400,
        "Can't execute this action. Not enough money or actions available "
      )
    )
    // Should fail because not enough money left
    expect(() => {
      actionService.updateMoney(1, { actions: 2, money: 3 }, game)
    }).toThrow(
      new HttpError(
        400,
        "Can't execute this action. Not enough money or actions available "
      )
    )
  })
})

describe("isFinished", () => {
  test("isFinished : should return true if the building is finished, if not, return false", async () => {
    expect(actionService.isFinished(1, 100, fakeGame2)).toStrictEqual(true)
    expect(actionService.isFinished(2, 101, fakeGame2)).toStrictEqual(false)
  })
})

describe("handleFinishedBuilding", () => {
  test(` handleFinishedBuilding : 
    should move the building once finished,
    should reward the player, 
    should give him back his workers 
    and if the building is a machine, 
    add it to the workers`, async () => {
    // simple building
    expect(
      actionService.handleFinishedBuilding(1, 100, fakeGame2).players[0]
    ).toStrictEqual({
      id: 1,
      victoryPoints: 2,
      money: 5,
      finishedBuildings: [
        {
          id: 100,
          reward: 5,
          victoryPoint: 2,
          stone: 0,
          wood: 0,
          knowledge: 0,
          tile: 0,
          stoneProduced: 0,
          woodProduced: 0,
          knowledgeProduced: 0,
          tileProduced: 0,
          workers: [],
          numberOfTimes: 0
        }
      ],
      availableWorkers: [worker],
      underConstructionBuildings: [
        {
          id: 101,
          reward: 0,
          victoryPoint: 2,
          stone: 0,
          wood: 0,
          knowledge: 0,
          tile: 0,
          stoneProduced: 0,
          woodProduced: 3,
          knowledgeProduced: 0,
          tileProduced: 0,
          workers: [worker2],
          numberOfTimes: 0
        }
      ]
    })
    // with a machine
    expect(
      actionService.handleFinishedBuilding(1, 101, fakeGame2).players[0]
    ).toStrictEqual({
      id: 1,
      victoryPoints: 4,
      money: 5,
      finishedBuildings: [
        {
          id: 100,
          reward: 5,
          victoryPoint: 2,
          stone: 0,
          wood: 0,
          knowledge: 0,
          tile: 0,
          stoneProduced: 0,
          woodProduced: 0,
          knowledgeProduced: 0,
          tileProduced: 0,
          workers: [],
          numberOfTimes: 0
        },
        {
          id: 101,
          reward: 0,
          victoryPoint: 2,
          stone: 0,
          wood: 0,
          knowledge: 0,
          tile: 0,
          stoneProduced: 0,
          woodProduced: 3,
          knowledgeProduced: 0,
          tileProduced: 0,
          workers: [],
          numberOfTimes: 0
        }
      ],
      availableWorkers: [
        worker,
        worker2,
        { id: 101, price: 0, stone: 0, wood: 3, knowledge: 0, tile: 0 }
      ],
      underConstructionBuildings: []
    })
  })
})

describe("updateVictoryPointsWithCoins", () => {
  test("Should update the victory points", async () => {
    const fakeGame3 = {
      players: [
        {
          id: 1,
          victoryPoints: 0,
          money: 23
        },
        {
          id: 2,
          victoryPoints: 0,
          money: 2
        }
      ]
    }
    actionService.updateVictoryPointsWithCoins(fakeGame3)
    expect(fakeGame3).toStrictEqual({
      players: [
        {
          id: 1,
          victoryPoints: 2,
          money: 23
        },
        {
          id: 2,
          victoryPoints: 0,
          money: 2
        }
      ]
    })
  })
})
